# Based on https://github.com/linuxserver/docker-libresonic/blob/master/Dockerfile
FROM openjdk:8-jdk-alpine
MAINTAINER jskov@mada.dk

ARG BUILD_DATE
ARG VERSION
LABEL build_version="Libresonic version:- ${VERSION} Build-date:- ${BUILD_DATE}"

ARG JETTY_VER="9.3.14.v20161028"

ENV LIBRE_HOME="/app/libresonic"
ENV LIBRE_SETTINGS="/config"


RUN apk update \
 && apk add --no-cache --virtual=build-dependencies \
	curl \
 && apk add --no-cache \
	ffmpeg \
	flac \
	lame \
	openjdk8-jre \
        ttf-dejavu

RUN curl -o \
      /tmp/"jetty-runner-$JETTY_VER".jar -L \
      "https://repo.maven.apache.org/maven2/org/eclipse/jetty/jetty-runner/${JETTY_VER}/jetty-runner-{$JETTY_VER}.jar" \
 && install -m644 -D /tmp/"jetty-runner-$JETTY_VER.jar" \
      /usr/share/java/jetty-runner.jar

ADD build/clone/libresonic-main/target/libresonic.war /app/
ADD start.sh /

RUN apk del --purge build-dependencies \
 && rm -rf /tmp/*

USER nobody

EXPOSE 8080
VOLUME /config /media /music /music2 /playlists /podcasts

#ENTRYPOINT ["/opt/netdata/usr/sbin/netdata"]
CMD ["/bin/sh", "/start.sh"]

