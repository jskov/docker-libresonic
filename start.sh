#!/bin/sh

mkdir -p "${LIBRE_SETTINGS}"/transcode

ln -sf /usr/bin/ffmpeg "${LIBRE_SETTINGS}"/transcode/
ln -sf /usr/bin/flac "${LIBRE_SETTINGS}"/transcode/
ln -sf /usr/bin/lame "${LIBRE_SETTINGS}"/transcode/

URL_BASE=/

java -Dlibresonic.home="${LIBRE_SETTINGS}" \
	-Dlibresonic.defaultMusicFolder=/music \
	-Dlibresonic.defaultPodcastFolder=/podcasts \
	-Dlibresonic.defaultPlaylistFolder=/playlists \
	-jar /usr/share/java/jetty-runner.jar \
	--host 0.0.0.0 --port 8080 --path "${URL_BASE}" \
	/app/libresonic.war
